### 1.Writing Docker Files
- Three separate docker files were created.
- They can be found at the link: https://gitlab.com/mrneverman/gitlab-cicd/-/tree/master/DockerFiles
- Following tools are used during writing of Dockerfiles:
    - [Haskell Dockerfile Linter (hadolint)](https://github.com/hadolint/hadolint "Haskell Dockerfile Linter (hadolint)"): it is a Dockerfile linter that helps you build best practice Docker images.
    - [Trivy](https://github.com/aquasecurity/trivy "Trivy"): it is a scanner for vulnerabilities in container images.
    - [Dive](https://github.com/wagoodman/dive "Dive"): it is a tool for exploring a docker image, layer contents, and discovering ways to shrink the size of your Docker/OCI image.
	
##### Dockerfile.api :
- It contains the titanic-api
- It is designed as a multi-stage builds to keep the final image size at minimum.  
- python:3.9.13-slim-bullseye is chosen as a minimal base image.
- In "builder" stage: 
    - Source code of titanic-api is downloaded by using its permanent link in order to avoid any future changes in titanic-api source causing unintended functionality problems.
    - Python virtual environment is created to install the python dependencies.
- In "run" stage:
   - A non-root user called "apiuser" is created to run container with non-root user.
   - Only required source code to run titanic-api is copied from builder stage to run stage.
   - Python virtual environment  is copied from builder stage to run stage.
   - Environment variable called "DATABASE_URL" will be overwritten during kubernetes deployment 
   
##### Dockerfile.db :
- It contains the sql database.
- postgres:14.3-bullseye is used as a base image.
- titanic.sql is added under the directory called "/docker-entrypoint-initdb.d/" via permanent link to initialize the database during start-up.
- "COPY" command added at the end of the titanic.sql file to populate the database with the values in titanic.csv file during start-up.
- Container will be run with non-root user during kubernetes deployment.
- Value of POSTGRES_USER and POSTGRES_PASSWORD will be override in kubernetes deployment stage by using kubernetes secret.

##### Dockerfile.test :
- It contains the unit-test for titanic-api.
- It is written in bash language to solely demonstrate fully functional CI pipelines. 
- More complex and higher code coverage rate test suites can be developed in open-source or commercial testing platforms. An example for python based test platform:  [py-test/tavern](https://github.com/taverntesting/tavern).
- Unit-test includes following stages:
  - Smoke Test.
  - Check number of entries in database via titanic-api.
  - Add new entry to database via titanic-api and get UUID of it.
  - Get details of new entry in database via titanic-api.
  - Update name field in new entry and get updated details of it
  - Delete newly added entry from database via titanic-api. 
  - Sample test reports can be found at the link: https://gitlab.com/mrneverman/gitlab-cicd/-/tree/master/SampleTestReports
- Commands to run unit test can be found below. Please do use the "latest" tag with care
```shell
DOCKER_BUILDKIT=1 docker build --no-cache -t registry.gitlab.com/mrneverman/gitlab-cicd/api:latest -f ./DockerFiles/Dockerfile.api .
DOCKER_BUILDKIT=1 docker build --no-cache -t registry.gitlab.com/mrneverman/gitlab-cicd/db:latest -f ./DockerFiles/Dockerfile.db .
DOCKER_BUILDKIT=1 docker build --no-cache -t registry.gitlab.com/mrneverman/gitlab-cicd/test:latest -f ./DockerFiles/Dockerfile.test .

docker rm -f $(docker ps -a -q)

docker run --name db -d -p 5432:5432 registry.gitlab.com/mrneverman/gitlab-cicd/db:latest
db_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' db)
echo $db_IP
docker run --name api -d -p 5000:5000 -e "DATABASE_URL=postgresql+psycopg2://user:password@$db_IP:5432/postgres" registry.gitlab.com/mrneverman/gitlab-cicd/api:latest
docker run --name test --net=host -e API_IP=0.0.0.0 registry.gitlab.com/mrneverman/gitlab-cicd/test:latest
```

### 2. Build and Push Docker Image to Gitlab Private Container Registry
In order to login the Gitlab Private Container Registry, docker login command is used. Detailed information can be found at the [Gitlab documentation page](https://docs.gitlab.com/ee/user/packages/container_registry/ "Gitlab documentation page").
Commands to build and push and docker image can be found below. Please do use the "latest" tag with care
```shell
docker login registry.gitlab.com
DOCKER_BUILDKIT=1 docker build --no-cache -t registry.gitlab.com/mrneverman/gitlab-cicd/api:latest -f ./DockerFiles/Dockerfile.api .
docker push registry.gitlab.com/mrneverman/gitlab-cicd/api:latest
```
    
### 3.Deploying the Application On Kubernetes
- Yaml files are written for deploying the application on Kubernetes. All of the Kubernetes resources are tested by deploying them on NEVERLAND. 
- Please see below for NEVERLAND: The Kubernetes Cluster
- They can be found at the link: https://gitlab.com/mrneverman/gitlab-cicd/-/tree/master/k8s
- Kubernetes secrets are used to store sensitive data such as postgres username and password. Using HashiCorp Vault would be better choice in terms of security, if it is supported by Kubernetes cluster.
- Kubernetes config are used to store configuration data.
- Both resource request and limits are defined for each deployment for Kubernetes QoS class of Guaranteed.
- Security Context are defined for each deployment to run containers with non-root user and prevent privilege escalation.
- Kubernetes network policy is created so that db pod can be accessed only by titanic-api pod in same namespace.
- Liveness and readiness probes added for both db and titanic-api deployment.
- A label called "deploy_version" added to both db and titanic-api deployment for the possible future use of different deployment strategies such as Blue-Green, Canary, and more.
- Kubernetes job called "api-test" created for the unit-test in cluster.
- Specifying a replica count greater than 1, podAntiAffinity and PodDisruptionBudget is suggested for api deployment. 
- Since db deployment is stateful, it will be more tricky to increase replica count. K8s operators can be considered in accordance with selected databases or a docker base image which allows master/slave configuration.
- Kubernetes yaml files are used as a baseline to develop titanic-api helm chart so Kubernetes yaml files may not be at the most updated versions. Please check helm directory for latest [helm chart dump](https://gitlab.com/mrneverman/gitlab-cicd/-/blob/master/helm/titanic-api_helmChartDump.yaml "helm chart dump"). 
- In order to access and pull container images from Gitlab Private Container Registry, imagePullSecrets should be created. Detailed information can be found at [Kubernetes documentation page.](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/ "Kubernetes documentation page.")
- In order to create imagePullSecrets, deploy tokens should be created for read-only access to Gitlab Private Container Registry.
Detailed information can be found at the [Gitlab documentation page](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html).
- In case you would like to try it on your environment, you can found username and token as: [username: "neverman"; access_token: "b9iQQKw3Gzqx26xs6-1H"] Please keep it secret. 
- Commands to access and pull container images from Gitlab Private Container Registry in Kubernetes can be found below:

```bash
 export REGISTRY_USERNAME= username
 export REGISTRY_PASSWORD= access_token
 export BASE_64_BASIC_AUTH_CREDENTIALS=$(echo -n "$REGISTRY_USERNAME:$REGISTRY_PASSWORD" | base64)
 touch .dockerconfigjson

 cat << EOF > .dockerconfigjson
 {
     "auths": {
         "https://registry.gitlab.com": {
             "username":"$REGISTRY_USERNAME",
             "password":"$REGISTRY_PASSWORD",
             "auth":"$BASE_64_BASIC_AUTH_CREDENTIALS"
         }
     }
 }
 EOF
 
export BASE_64_ENCODED_DOCKER_FILE=$(cat .dockerconfigjson | base64)

cat <<EOF | kubectl apply -f -
 apiVersion: v1
 kind: Secret
 metadata:
   name: registry-credentials
   namespace: default
 type: kubernetes.io/dockerconfigjson
 data:
   .dockerconfigjson: $BASE_64_ENCODED_DOCKER_FILE
 EOF
```
### 4. NEVERLAND: The Kubernetes Cluster
Neverland is a self managed production ready kubernetes cluster project which works in fully automated fashion with Terraform and Ansible. It is solely developed by me and it lives in Google Cloud Platform.  I started to create and develop Neverland at the end of January 2022. At the time of this writing, AN.3 release of Neverland was available at https://github.com/mrneverman/NEVERLAND. There is also a demonstration video of Neverland called "Neverland in action: AN.3". I will be really happy if you have some time to look at its github page and "Neverland in action" video(https://gitlab.com/mrneverman/gitlab-cicd/-/blob/master/NEVERLAND%20Demo%20Video/Neverland_in_action__AN.3.mp4).
![](https://raw.githubusercontent.com/mrneverman/NEVERLAND/main/drawings/NEVERLAND_architecture.png)
Features of NEVERLAND.
- It lives in Google Cloud Platform as a self-managed cluster. Not using GKE!.
- It is fully configurable with Terraform and Ansible. Number of control-plane nodes, infrastructure nodes and worker nodes can be changed arbitrary. 
- [HAProxy](http://www.haproxy.org/ "HAProxy") serves as a layer-7 reverse proxy and load balancer.
- Istio service mesh and Kiali are available.
- Prometheus and Grafana are available.
- Goldilocks can be used with [Locust](https://locust.io/ "Locust") to set cpu and memory resource request and limits for pods.

### 5. Helm Charts
- titanic-api helm chart is created to ease the deployment of application.
- Latest titanic-api helm chart can be found at link: https://gitlab.com/mrneverman/gitlab-cicd/-/tree/master/helm/titanic-api
- Please check helm directory for latest [helm chart dump](https://gitlab.com/mrneverman/gitlab-cicd/-/blob/master/helm/titanic-api_helmChartDump.yaml "helm chart dump"). 
- For persistent storage of db data, kubernetes persistent volume claim is created. If it is desired, persistent volumes and storage class should be created and enabled in [values.yaml](https://gitlab.com/mrneverman/gitlab-cicd/-/blob/master/helm/titanic-api/values.yaml#L136) file of titanic-api helm chart in accordance with the selected cloud provider.
- Please do not use the "latest" tag for container images of pod and update it with a proper value in  [values.yaml](https://gitlab.com/mrneverman/gitlab-cicd/-/blob/master/helm/titanic-api/values.yaml) file.

### 6. Push and Pull titanic-api helm chart to Gitlab Private Package Registry.
- In order to Push and Pull titanic-api helm chart to Gitlab Private Package Registry, deploy tokens should be created for read-only access to Gitlab Private Package Registry. Detailed information can be found at the [Gitlab documentation page](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html).
- In case you would like to try it on your environment, you can found username and token as: [username: "neverman"; access_token: "o-zxzzWsHVRMwVkPKx9p"] Please keep it secret.
- Commands to push titanic-api helm chart to Gitlab Private Package Registry can be found below and at the [Gitlab documentation page](https://docs.gitlab.com/ee/user/packages/helm_repository/). :

```bash
cd helm
helm package titanic-api
curl --request POST \
     --form 'chart=@titanic-api-0.1.0.tgz' \
     --user <username>:<access_token> \
     https://gitlab.example.com/api/v4/projects/36370557/packages/helm/api/stable/charts
```
- Commands to pull titanic-api helm chart to Gitlab Private Package Registry can be found below and at the [Gitlab documentation page](https://docs.gitlab.com/ee/user/packages/helm_repository/). :

```bash
helm repo add --username <username> --password <access_token> GitLabRepo https://gitlab.com/api/v4/projects/36370557/packages/helm/stable

helm search repo
-> NAME                                                    CHART VERSION   APP VERSION     DESCRIPTION
-> GitLabRepo/titanic-api                                  0.1.0           1.16.0          A Helm chart for titanic-api

```
### 7. Implement a CI/CD Pipeline
- CI/CD pipeline is created using Gitlab CI/CD
- Latest Gitlab CI/CD pipeline can be found at link: https://gitlab.com/mrneverman/gitlab-cicd/-/blob/master/.gitlab-ci.yml
- CI/CD pipeline consists following stages:
  - DockerfileLintCheck: Hadolint is used for lint checking of DockerFile and store results in Gitlab artifacts
  - build: Build Dockerfiles and push it with the tag of [$CI_COMMIT_SHORT_SHA](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html "$CI_COMMIT_SHORT_SHA")
  - test: 
    - Scan builded images with Trivy for vulnerabilities and store results in Gitlab artifacts
    - Unit-test is applied as described in Dockerfile.test section of this document
  - push: 
    - latest: Tag and push the image with "latest" tag if it is only from "master" branch and passed the "test" stage.
    - tag: Tag and push the image whenever the new Git tag created. Creation of new tag initialize the whole pipeline including DockerfileLintCheck, build and test.
  - HelmLintCheck: "helm lint" is used for lint checking of titanic-api helm chart and store results in Gitlab artifacts
  - HelmIntegrationTest: A kubernetes cluster in docker is created using [Kind](https://kind.sigs.k8s.io/) for testing. Kubernetes jobs called "api-test" runs for integration test.
  - HelmPublish: Push helm chart to Gitlab Private Package Registry.
  
  
- Continuous Delivery: Since the CD part should be considered with the targeted cluster, I did not implemented it however i consider that it is possible to implement it in several ways:
     - Connect Gitlab to one of the supported "managed kubernetes services" such as Google GKE or Amazon EKS and enable GitLab Auto DevOps.
     - Install the Gitlab agent on self-managed kubernetes cluster. Select one of the flows either [the GitOps workflow or the CI/CD workflow](https://docs.gitlab.com/ee/user/clusters/agent/#workflows)
     - Implementation of CD pipelines on cluster either using ArgoCD or Flux. Implementation of CD pipeline is in roadmap of NEVERLAND and I am targeting to complete it in AN.5 release.

