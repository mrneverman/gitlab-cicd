{{/*
Expand the name of the chart.
*/}}
{{- define "titanic-api.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "titanic-api.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "titanic-api.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels for api
*/}}
{{- define "titanic-api.api.labels" -}}
helm.sh/chart: {{ include "titanic-api.chart" . }}
{{ include "titanic-api.api.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
environment: {{ .Values.api.labels.environment }}
role: {{ .Values.api.labels.role }} 
deploy_version: {{ .Values.api.labels.deploy_version }}
{{- end }}

{{/*
Selector labels for api
*/}}
{{- define "titanic-api.api.selectorLabels" -}}
app.kubernetes.io/name: {{ .Values.api.AppName }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Common labels for db
*/}}
{{- define "titanic-api.db.labels" -}}
helm.sh/chart: {{ include "titanic-api.chart" . }}
{{ include "titanic-api.db.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
environment: {{ .Values.db.labels.environment }}
role: {{ .Values.db.labels.role }}
deploy_version: {{ .Values.db.labels.deploy_version }}
{{- end }}

{{/*
Selector labels for db
*/}}
{{- define "titanic-api.db.selectorLabels" -}}
app.kubernetes.io/name: {{ .Values.db.AppName }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use in api
*/}}
{{- define "titanic-api.api.serviceAccountName" -}}
{{- if .Values.api.serviceAccount.create }}
{{- default (include "titanic-api.fullname" .) .Values.api.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.api.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use in db
*/}}
{{- define "titanic-api.db.serviceAccountName" -}}
{{- if .Values.db.serviceAccount.create }}
{{- default (include "titanic-api.fullname" .) .Values.db.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.db.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create environment variable for api
*/}}

{{- define "titanic-api.api.list-env-variables-from-secret"}}
{{- range $key, $val := .Values.api.env.secret }}
- name: {{ $key }}
  valueFrom:
    secretKeyRef:
      name: {{ $.Values.secret.name }}
      key: {{ $key }}
{{- end}}
{{- end }}

{{- define "titanic-api.api.list-env-variables-from-config"}}
{{- range $key, $val := .Values.api.env.config }}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      name: {{ $.Values.config.name }}
      key: {{ $key }}
{{- end}}
{{- end }}

{{/*
Create environment variable for db
*/}}

{{- define "titanic-api.db.list-env-variables-from-secret"}}
{{- range $key, $val := .Values.db.env.secret }}
- name: {{ $key }}
  valueFrom:
    secretKeyRef:
      name: {{ $.Values.secret.name }}
      key: {{ $key }}
{{- end}}
{{- end }}

{{- define "titanic-api.db.list-env-variables-from-config"}}
{{- range $key, $val := .Values.db.env.config }}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      name: {{ $.Values.config.name }}
      key: {{ $key }}
{{- end}}
{{- end }}

{{- define "imagePullSecret" }}
{{- with .Values.imageCredentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}
