#!/bin/bash
printf "############################################################\n"
printf "#################### Smoke Test ############################\n"
printf "############################################################\n"
if curl --fail -s http://$API_IP:5000/people> /dev/null; then
	curl -s http://$API_IP:5000/people | jq '.'
	printf "############################################################\n"
	printf "################## Smoke Test: Passed ######################\n"
	printf "############################################################\n"
else
	>&2 printf "############################################################\n"
        printf "################## Smoke Test: Failed ######################\n"
        printf "############################################################\n"
        exit 1
fi

printf "\n############################################################\n"
printf "########### Check number of items in DB ####################\n"
printf "############################################################\n"
CurlReturnNumber=$(curl -s http://$API_IP:5000/people |grep -o name|wc -l)
CompareNumber=887
if [ $CurlReturnNumber -ne $CompareNumber ]; then
        >&2 printf "############################################################\n"
        printf "#######  Check number of items in DB: Failed ###############\n"
        printf "############################################################\n"
	printf "number of entries in DB:%s\n" $CurlReturnNumber
	printf "number of initial values:%s\n" $CompareNumber
  	exit 1
else
        printf "############################################################\n"
        printf "#######  Check number of items in DB: Passed ###############\n"
        printf "############################################################\n"
	printf "number of entries in DB:%s\n" $CurlReturnNumber
	printf "number of initial values:%s\n" $CompareNumber
fi

printf "\n############################################################\n"
printf "#################### Add new person ########################\n"
printf "############################################################\n"
UUID=$(curl -s -H "Content-Type: application/json" -X POST http://$API_IP:5000/people -d'{"survived": 2,"passengerClass": 2,"name": "MrNeverman", "sex": "male","age": 22.0,"siblingsOrSpousesAboard": 4,"parentsOrChildrenAboard": 5,"fare": 7.25}' | jq -r ".uuid")
CurlReturnNumber1=$(curl -s http://$API_IP:5000/people |grep -o MrNeverman|wc -l)
CompareNumber1=1
CurlReturnNumber2=$(curl -s http://$API_IP:5000/people |grep -o name|wc -l)
CompareNumber2=888
if [ $CurlReturnNumber1 -ne $CompareNumber1 ] && [ $CurlReturnNumber2 -ne $CompareNumber2 ] ; then
        >&2 printf "############################################################\n"
        printf "###############  Add new person: Failed ####################\n"
        printf "############################################################\n"
        exit 1
else
        printf "############################################################\n"
        printf "###############  Add new person: Passed ####################\n"
        printf "############################################################\n"
	printf "UUID of new person: %s\n" $UUID
fi

printf "\n############################################################\n"
printf "############## Get details of new person  ###################\n"
printf "############################################################\n"
if curl --fail -s -H "Content-Type: application/json" -X GET http://$API_IP:5000/people/$UUID > /dev/null; then
        printf "############################################################\n"
        printf "##########  Get details of new person:: Passed #############\n"
        printf "############################################################\n"

	printf "New Person:\n"
	curl -s -H "Content-Type: application/json" -X GET http://$API_IP:5000/people/$UUID| jq '.'
else
        >&2 printf "############################################################\n"
        printf "##########  Get details of new person: Failed ##############\n"
        printf "############################################################\n"
        exit 1
fi

printf "\n############################################################\n"
printf "################## Update new person #######################\n"
printf "############################################################\n"
if curl -s -H "Content-Type: application/json" -X PUT http://$API_IP:5000/people/$UUID -d'{"survived": 2,"passengerClass": 2,"name": "Neverland", "sex": "male","age": 22.0,"siblingsOrSpousesAboard": 4,"parentsOrChildrenAboard": 5,"fare": 7.25}'  > /dev/null; then
    CurlReturn=$(curl -s -H "Content-Type: application/json" -X GET http://$API_IP:5000/people/$UUID)
    CompareNumber3=$(echo $CurlReturn|grep -o MrNeverman|wc -l)
    CompareNumber4=$(echo $CurlReturn|grep -o Neverland|wc -l)
    if [ $CompareNumber3 -eq 0 ] && [ $CompareNumber4 -eq 1 ]; then
           printf "############################################################\n"
           printf "#############  Update new person: Passed ###################\n"
           printf "############################################################\n"
	   printf "Updated Person:\n"
	   curl -s -H "Content-Type: application/json" -X GET http://$API_IP:5000/people/$UUID| jq '.'
    else
	   >&2 printf "############################################################\n"
	   printf "#####  Update new person / Not Updaded: Failed #############\n"
	   printf "############################################################\n"
	   exit 1
    fi
else
    >&2 printf "############################################################\n"
    printf "######  Update new person / Connection Error: Failed #######\n"
    printf "############################################################\n"
    exit 1
fi

printf "\n############################################################\n"
printf "################## Delete new person #######################\n"
printf "############################################################\n"
if curl --fail -s -H "Content-Type: application/json" -X DELETE http://$API_IP:5000/people/$UUID > /dev/null; then
    CurlReturn=$(curl -s -H "Content-Type: application/json" -X GET http://$API_IP:5000/people)
    CompareNumber5=$(echo $CurlReturn|grep -o Neverland|wc -l)
    if [ $CompareNumber5 -eq 0 ]; then
           printf "############################################################\n"
           printf "############## Delete new person: Passed ###################\n"
           printf "############################################################\n"
    else
           >&2 printf "############################################################\n"
           printf "############ Delete new person / Not Deleted: Failed #######\n"
           printf "############################################################\n"
           exit 1
    fi
else
    >&2 printf "############################################################\n"
    printf "####### Delete new person / Connection Error: Failed #######\n"
    printf "############################################################\n"
    exit 1
fi

printf "\n\n############################################################\n"
printf "################# All Test : Passed ########################\n"
printf "############################################################\n\n"

